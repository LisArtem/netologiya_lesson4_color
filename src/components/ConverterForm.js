
import React, {useState} from 'react';


const hex2rgb = hex => {
    if (hex[0] !== '#' || hex.length > 7) return 'Ошибка!';
    if (hex.length < 7) return null;
    if (hex[0] === '#') {
        const r = parseInt(hex.substring(1, 3), 16);
        const g = parseInt(hex.substring(3, 5), 16);
        const b = parseInt(hex.substring(5, 7), 16);
        return isNaN(r + g + b) || r > 255 || g > 255 || b > 255 ? 'Ошибка!' : `rgb(${r},${g},${b})`;
    }
};

const ConverterForm = () => {
    const [rgb, setRgb] = useState({value: ''});

    const handleInput = e => {
        const rgb = hex2rgb(e.target.value);
        setRgb(prevRgb => ({...prevRgb, value: rgb}));
    };

    return (
        <div className="form-container" style={{backgroundColor: rgb.value && rgb.value !== 'Ошибка!' ? rgb.value : null}}>
<form>
    <input className="form__input" name="color" onChange={handleInput} type="text"/>
        <div className="form__info" style={{ backgroundColor: rgb.value && rgb.value !== 'Ошибка!' ? rgb.value : null}}>{rgb.value}</div>
    </form>
    </div>
)
};

export default ConverterForm;