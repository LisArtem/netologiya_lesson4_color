import React from 'react';
import ConverterForm from './components/ConverterForm';
import './App.css';

function App() {
    return (
        <div className="App">
        <ConverterForm/>
        </div>
);
}

export default App;